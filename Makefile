all:
	make -C Utils
	make -C Communication
	make -C Test

test: all
	Test/bin/Test

clean:
	make -C Utils clean
	make -C Communication clean
	make -C Test clean

cleaner:
	make -C Utils cleaner
	make -C Communication cleaner
	make -C Test cleaner