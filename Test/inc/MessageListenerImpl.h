#pragma once

#include <IMessageListener.h>
#include <Message.h>
#include <SocketChannel.h>
#include <log.h>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

class MessageListenerImpl : public ISimpleMessageListener {
 public:
  inline void OnMessage(const Message& message) {
    std::unique_lock<std::mutex> lk(mtx);
    // LOG(INFO) << "On listener message: " << message.GetContent();
    received.push_back(message.GetContent());
    cv.notify_one();
  }

  inline std::string ExpectMessage(int TimeoutMs) {
    std::unique_lock<std::mutex> lk(mtx);
    // LOG(INFO) << "Received count: " << received.size();
    cv.wait_for(lk, std::chrono::milliseconds(TimeoutMs),
                [this] { return !received.empty(); });

    if (received.empty()) {
      throw std::runtime_error("Expect Message timed out");
    }

    std::string retVal = received[0];
    received.erase(received.begin());
    return retVal;
  }

 private:
  std::vector<std::string> received;
  std::mutex mtx;
  std::condition_variable cv;
};
