#include <SocketChannel.h>
#include <SocketException.h>
#include <log.h>
#include <future>
#include "MessageListenerImpl.h"
#include "gtest/gtest.h"

namespace {
int LOCAL_PORT = 6000;   // Will be incremented for each test
int REMOTE_PORT = 6500;  // Will be incremented for each test

const int TIMEOUT_MS = 5 * 1000;
const char* ADDRESS = "localhost";
}

void Increment() {
  LOCAL_PORT++;
  REMOTE_PORT++;
}

TEST(SocketChannelTest, Test_Init) {
  Increment();
  SocketChannel channel(LOCAL_PORT);
  channel.Start();

  // Disabled until #36 (Implement replacement SocketChannel promise) is fixed
  // SocketChannel duplicate(LOCAL_PORT);
  // ASSERT_THROW(duplicate.Start(), SocketException);
}

TEST(SocketChannelTest, Test_Send) {
  Increment();
  SocketChannel local(LOCAL_PORT);
  local.Start();

  std::shared_ptr<MessageListenerImpl> listener =
      std::make_shared<MessageListenerImpl>();

  SocketChannel remote(REMOTE_PORT);
  remote.AddListener(listener);
  remote.Start();

  std::this_thread::sleep_for(
      std::chrono::milliseconds(100));  // Give server time to set up

  std::string sentMessage = "Testing 1 2 3";
  local.Send(Message(sentMessage), ADDRESS, REMOTE_PORT);
  std::string secondMessage = "Testing 5 6";
  local.Send(Message(secondMessage), ADDRESS, REMOTE_PORT);

  ASSERT_EQ(sentMessage, listener->ExpectMessage(TIMEOUT_MS));
  ASSERT_EQ(secondMessage, listener->ExpectMessage(TIMEOUT_MS));

  std::string largeString(2048, 'a');
  local.Send(Message(largeString), ADDRESS, REMOTE_PORT);
  ASSERT_EQ(largeString, listener->ExpectMessage(TIMEOUT_MS));

  ASSERT_THROW(local.Send(Message(""), ADDRESS, REMOTE_PORT),
               std::invalid_argument);
}

TEST(SocketChannelTest, Test_SendWReply) {
  Increment();
  SocketChannel local(LOCAL_PORT);
  local.Start();

  std::shared_ptr<MessageListenerImpl> listener =
      std::make_shared<MessageListenerImpl>();

  SocketChannel remote(REMOTE_PORT);
  remote.AddListener(listener);
  remote.Start();

  std::this_thread::sleep_for(
      std::chrono::milliseconds(100));  // Give server time to set up

  std::string sentMessage = "Testing 1 2 3";
  std::string reply =
      local.SendWReply(Message(sentMessage), ADDRESS, REMOTE_PORT).GetContent();
  ASSERT_EQ("ack", reply);
}
