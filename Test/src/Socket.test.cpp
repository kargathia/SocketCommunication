#include <SocketClient.h>
#include <SocketException.h>
#include <SocketServer.h>
#include <log.h>
#include <unistd.h>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdlib>
#include <future>
#include <mutex>
#include <thread>
#include <vector>
#include "gtest/gtest.h"

namespace {
int SERVER_PORT = 9000;  // Will be incremented for each test to avoid testing
                         // on recently used ports

const char* SERVER_IP = "localhost";
const int SHORT_TIMEOUT_MS = 100;
}

class SocketTest : public ::testing::Test {
 protected:
  SocketClient client;
  SocketServer server;
  std::vector<std::string> received;

  virtual void SetUp() {
    SERVER_PORT++;
    using std::placeholders::_1;
    using std::placeholders::_2;
    server.SetMessageHandler(
        std::bind(&SocketTest::TestMessageHandler, this, _1, _2));
  }

  virtual void TearDown() {
    EndPolling();
  }

  bool PollSocket() {
    bool result = true;
    isPolling = true;
    if (!server.Bind(SERVER_PORT)) {
      return false;
    }
    while (isPolling) {
      result = result && server.Poll(SHORT_TIMEOUT_MS);
      hasPolled = true;
      polledCV.notify_all();
    }

    return result;
  }

  void StartPolling() {
    pollResult = std::async(std::launch::async, &SocketTest::PollSocket, this);

    std::this_thread::sleep_for(
        std::chrono::milliseconds(100));  // Give server time to set up
  }

  bool EndPolling() {
    isPolling = false;
    polledCV.notify_all();
    return pollResult.valid() && pollResult.get();
  }

  bool WaitPolled(int count = 2) {
    for (int i = 0; i < count; ++i) {
      std::mutex m;
      std::unique_lock<std::mutex> lk(m);
      hasPolled = false;
      polledCV.wait_for(lk, std::chrono::milliseconds(5000),
                        [this] { return hasPolled && isPolling; });
    }

    return hasPolled;
  }

  std::string TestMessageHandler(int fDesc, std::string content) {
    LOG(INFO) << "Received: " << content;
    received.push_back(content);
    return "ack";
  }

 private:
  std::atomic_bool isPolling;
  std::atomic_bool hasPolled;
  std::future<bool> pollResult;
  std::condition_variable polledCV;
};

TEST_F(SocketTest, Test_Server) {
  ASSERT_TRUE(server.Bind(SERVER_PORT));
  ASSERT_FALSE(server.Bind(SERVER_PORT));
  ASSERT_TRUE(server.IsValid());

  SocketServer duplicate;
  ASSERT_FALSE(duplicate.Bind(SERVER_PORT));
}

TEST_F(SocketTest, Test_BlankPoll) {
  ASSERT_TRUE(server.Bind(SERVER_PORT));
  ASSERT_TRUE(server.Poll(SHORT_TIMEOUT_MS));
}

TEST_F(SocketTest, Test_Connect) {
  StartPolling();
  ASSERT_TRUE(client.Connect(SERVER_IP, SERVER_PORT));
  ASSERT_TRUE(EndPolling());
}

TEST_F(SocketTest, Test_Send) {
  StartPolling();
  ASSERT_TRUE(client.Connect(SERVER_IP, SERVER_PORT));
  std::string sent = "Nobody expects the Spanish Inquisition!";
  ASSERT_TRUE(client.Send(sent));

  // Append Data
  for (int i = 0; i < 10; ++i) {
    std::string appended = " " + std::to_string(i);
    ASSERT_TRUE(client.Send(appended));
    sent += appended;
  }

  ASSERT_FALSE(client.Send(""));
  WaitPolled();
  ASSERT_EQ(sent, received.at(0));

  std::string largeStr(2048, ' ');
  ASSERT_TRUE(client.Send(largeStr));
  WaitPolled();
  ASSERT_EQ(largeStr, received.at(1));

  SocketClient dummy;
  ASSERT_FALSE(dummy.Send("Should have connected first"));
  ASSERT_TRUE(EndPolling());
}

TEST_F(SocketTest, Test_SendWReply) {
  StartPolling();
  ASSERT_TRUE(client.Connect(SERVER_IP, SERVER_PORT));
  ASSERT_TRUE(client.Send("Hello, anybody home?!"));
  std::string reply;
  bool replied = false;
  for (int i = 0; (i < 10 && !replied); ++i) {
    replied = client.GetReply(&reply);
    LOG(INFO) << "GetReply attempt " << i << " timed out";
  }
  ASSERT_TRUE(replied);
  ASSERT_EQ(reply, "ack");
  ASSERT_TRUE(EndPolling());
}
