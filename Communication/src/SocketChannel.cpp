#include <log.h>
#include <unistd.h>
#include <iostream>
#include <memory>
#include <thread>
#include "ICommunicator.h"
#include "IMessageListener.h"
#include "Message.h"
#include "SocketClient.h"
#include "SocketException.h"
#include "SocketServer.h"

#include "SocketChannel.h"

namespace {
const int POLL_TIMEOUT_MS = 2 * 1000;
}

SocketChannel::SocketChannel(int localPort)
    : ICommunicator(), myPort(localPort), isRunning(false) {}

SocketChannel::~SocketChannel() {
  isRunning = false;
  myMessageCV.notify_all();

  if (myServerThread.joinable()) {
    myServerThread.join();
  }
  if (myHandlerThread.joinable()) {
    myHandlerThread.join();
  }
}

std::string SocketChannel::GetLocal() const {
  std::string local = myAddress + ":" + std::to_string(myPort);
  return local;
}

void SocketChannel::ClientConnect(SocketClient *client,
                                  const std::string &destination,
                                  int destinationPort) {
  if (!client->Connect(destination, destinationPort)) {
    std::string error = "Unable to connect to " + destination + ":" +
                        std::to_string(destinationPort);
    throw SocketException(error);
  }
}

void SocketChannel::ClientSend(SocketClient *client, const Message &message) {
  if (message.GetContent().empty()) {
    throw std::invalid_argument("Message content was empty");
  }
  if (!client->Send(message.GetContent())) {
    std::string error = "Unable to send message: " + message.GetContent();
    throw SocketException(error);
  }
}

void SocketChannel::Send(const Message &message, const std::string &destination,
                         int destinationPort) {
  SocketClient client = SocketClient();
  ClientConnect(&client, destination, destinationPort);
  ClientSend(&client, message);
}

Message SocketChannel::SendWReply(const Message &message,
                                  const std::string &destination,
                                  int destinationPort, int timeoutS) {
  SocketClient client = SocketClient();
  ClientConnect(&client, destination, destinationPort);
  ClientSend(&client, message);
  std::string replyContent;
  for (int i = 0; i < timeoutS; ++i) {
    if (client.GetReply(&replyContent)) {
      return Message(replyContent);
    }
  }
  throw SocketException("Timed out waiting for reply");
}

void SocketChannel::RunServer() {
  SocketServer server;
  using std::placeholders::_1;
  using std::placeholders::_2;
  server.SetMessageHandler(
      std::bind(&SocketChannel::HandleMessage, this, _1, _2));
  bool boundOk = server.Bind(myPort);
  while (boundOk && isRunning) {
    server.Poll(POLL_TIMEOUT_MS);
  }
}

void SocketChannel::RunClient() {
  try {
    while (isRunning) {
      NotifyListeners();
    }
  } catch (std::exception &ex) {
    LOG(ERROR) << "SocketChannel client thread terminated with error: "
               << ex.what();
  }
}

bool SocketChannel::WaitMessage(std::string *content) {
  std::unique_lock<std::mutex> lock(myMessageMtx);
  if (isRunning && myMessages.empty()) {
    myMessageCV.wait(lock,
                     [this] { return !isRunning || !myMessages.empty(); });
  }

  if (isRunning && !myMessages.empty()) {
    *content = myMessages[0];
    myMessages.erase(myMessages.begin());
    return true;
  }
  return false;
}

void SocketChannel::NotifyListeners() {
  std::string content;
  bool messageReceived = WaitMessage(&content);

  if (isRunning && !myQueuedListeners.empty()) {
    // We want to avoid modifying the listeners while they are being notified
    // But it is perfectly legal for functions called by OnMessage() to add a
    // listener
    // This way deadlocks are avoided, and the channel clearly guarantees that
    // the
    // new listener will not be called for the current message
    std::unique_lock<std::mutex> lock(myListenerQueueMtx);
    myListeners.insert(myListeners.end(), myQueuedListeners.begin(),
                       myQueuedListeners.end());
    myQueuedListeners.clear();
  }

  if (!isRunning || !messageReceived) {
    return;
  }

  Message message(content);
  std::vector<std::weak_ptr<ISimpleMessageListener>>::iterator it =
      myListeners.begin();

  while (it != myListeners.end()) {
    // Because we are using weak pointers, we need to check if they expired
    // before we used them
    if (std::shared_ptr<ISimpleMessageListener> listenerPtr = (*it).lock()) {
      try {
        listenerPtr->OnMessage(message);
      } catch (std::exception &ex) {
        LOG(ERROR) << "Exception caught in " << __func__ << ": " << ex.what();
      }
      ++it;
    } else {
      it = myListeners.erase(it);
    }
  }
}

void SocketChannel::AddListener(
    std::weak_ptr<ISimpleMessageListener> listener) {
  std::unique_lock<std::mutex> lock(myListenerQueueMtx);
  myQueuedListeners.push_back(listener);
}

std::string SocketChannel::HandleMessage(int fDesc, std::string message) {
  {  // We want to notify outside our lock
    std::unique_lock<std::mutex> lock(myMessageMtx);
    myMessages.push_back(message);
  }
  myMessageCV.notify_one();
  return "ack";
}

void SocketChannel::Start() {
  isRunning = true;
  myServerThread = std::thread(&SocketChannel::RunServer, this);
  myHandlerThread = std::thread(&SocketChannel::RunClient, this);
}
