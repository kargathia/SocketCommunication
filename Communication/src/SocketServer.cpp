#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "SocketClient.h"
#include "SocketException.h"
#include "SocketServer.h"

namespace {
const int ON = 1;
const int OFF = 0;

const int BACKLOG_SIZE = 20;
const int RETRY_ATTEMPTS = 40;
}

std::string DefaultMessageHandler(int fDesc, std::string content) {
  LOG(INFO) << "Message to default message handler: " << content;
  return "ack";
}

SocketServer::SocketServer()
    : numPolledFDs(1), myMessageHandler(DefaultMessageHandler) {
  ::memset(&polledFDs, 0, sizeof(polledFDs));
}

SocketServer::~SocketServer() {}

bool SocketServer::Bind(int port) {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = ::htons(port);

  if (!Configure(address)) {
    return false;
  }

  if (::setsockopt(GetFDesc(), SOL_SOCKET, SO_REUSEADDR, &ON, sizeof(ON)) < 0) {
    LOG(ERROR) << "Unable to turn on address reuse: " << GetErrString(errno);
    return false;
  }

  if (::bind(GetFDesc(), (sockaddr *)&address, sizeof(address)) < 0) {
    LOG(ERROR) << "Error binding: " << GetErrString(errno);
    return false;
  } else {
    SetAddr(address);
  }

  if (::listen(GetFDesc(), BACKLOG_SIZE) < 0) {
    LOG(ERROR) << "Error listening: " << GetErrString(errno);
    return false;
  }

  LOG(INFO) << "Successfully bound to port " << port;

  return true;
}

bool SocketServer::Poll(int timeoutMs) {
  // Ensure that this socket is always polled
  polledFDs[0].fd = GetFDesc();
  polledFDs[0].events = POLLIN;

  int resultCode = ::poll(polledFDs, numPolledFDs, timeoutMs);

  if (resultCode < 0) {  // error
    LOG(ERROR) << "Poll failed: " << GetErrString(errno);
    return false;
  }

  if (resultCode == 0) {  // timeout
    return true;
  }

  // Check if there are any sockets ready to be accepted
  if (polledFDs[0].revents > 0) {
    while (true) {
      int newFD = Accept();
      if (newFD > 0) {
        polledFDs[numPolledFDs].fd = newFD;
        polledFDs[numPolledFDs].events = POLLIN;
        polledFDs[numPolledFDs].revents = 0;
        numPolledFDs++;
      } else {
        break;
      }
    }
  }

  bool needsCompressing = false;
  // Loop through all polls descriptions
  for (int i(1); i < numPolledFDs; ++i) {
    pollfd *currentPoll = &polledFDs[i];

    if (currentPoll->revents == 0) {
      // Nothing to see here. Move along.
      continue;
    }

    if (currentPoll->revents & POLLHUP) {
      LOG(INFO) << "FD " << currentPoll->fd << " disconnected";
      ClosePolledFD(currentPoll);
      needsCompressing = true;
      continue;
    }

    if (currentPoll->revents & POLLERR) {
      LOG(INFO) << "FD " << currentPoll->fd << " had an error";
      ClosePolledFD(currentPoll);
      needsCompressing = true;
      continue;
    }

    if (currentPoll->revents & POLLIN) {
      std::stringstream ss;
      if (Read(currentPoll->fd, &ss)) {
        std::string message = ss.str();
        std::string reply = myMessageHandler(currentPoll->fd, message);
        Send(currentPoll->fd, reply);
      } else {
        ClosePolledFD(currentPoll);
      }
    }
  }
  if (needsCompressing) {
    CompressPolledArray();
  }
  return true;
}

void SocketServer::ClosePolledFD(pollfd *polled) {
  ::close(polled->fd);
  polled->fd = -1;
}

int SocketServer::Accept() {
  if (!CheckValid(GetFDesc())) {
    return false;
  }

  int fDesc = -1;
  struct sockaddr_in clientAddr;
  int clientAddrLen = sizeof((struct sockaddr *)&clientAddr);

  int attempt = 0;
  do {
    attempt++;
    fDesc = ::accept(GetFDesc(), (struct sockaddr *)&clientAddr,
                     (socklen_t *)&clientAddrLen);
  } while (fDesc < 0 && attempt < RETRY_ATTEMPTS && errno == EAGAIN);

  if (fDesc < 0 && errno != EAGAIN) {
    LOG(ERROR) << "Failed to accept socket: " << GetErrString(errno);
    return -1;
  }

  if (!CheckValid(fDesc)) {
    return -1;
  }
  Socket::AddressInfo info = GetPeerName(fDesc);
  LOG(INFO) << "Accepted FD " << fDesc << " from " << info.address << ":"
            << info.port;
  return fDesc;
}

void SocketServer::CompressPolledArray() {
  for (int i = 0; i < numPolledFDs; i++) {
    if (polledFDs[i].fd < 0) {
      for (int j = i; j < numPolledFDs; j++) {
        polledFDs[j].fd = polledFDs[j + 1].fd;
      }
      numPolledFDs--;
    }
  }
}
