#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <mutex>   //NOLINT
#include <thread>  //NOLINT
#include "Socket.h"
#include "SocketException.h"

namespace {
const int RECEIVE_TIMEOUT_S = 1;
const int SEND_TIMEOUT_S = 1;
const int READ_BUFFER_SIZE = 1024;

const int ON = 1;
const int OFF = 0;

std::mutex SocketCreateMutex;
}

std::string Socket::GetErrString(int err) const {
  std::stringstream ss;
  ss << strerror(err) << "(" << err << ")";
  return ss.str();
}

Socket::Socket() : myFDesc(-1) { ::memset(&myAddress, 0, sizeof(myAddress)); }

Socket::~Socket() { Disconnect(); }

void Socket::Disconnect() {
  if (myFDesc >= 0) {
    int retVal = ::close(myFDesc);
    LOG_IF((retVal < 0), WARNING) << "Failed to disconnect " << myFDesc << ": "
                                  << GetErrString(errno);
  }
  myFDesc = -1;
}

bool Socket::CheckValid(int fDesc) {
  return (fcntl(fDesc, F_GETFL) != -1 || errno != EBADF);
}

int Socket::Create() {
  std::lock_guard<std::mutex> lock(SocketCreateMutex);
  return ::socket(AF_INET, SOCK_STREAM, 0);
}

bool Socket::Configure() {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = 0;

  return Configure(address);
}

bool Socket::Configure(const sockaddr_in &address) {
  return Configure(address, Create());
}

bool Socket::Configure(const sockaddr_in &address, int fDesc) {
  if (!CheckValid(fDesc)) {
    return false;
  }

  myFDesc = fDesc;
  myAddress.sin_family = address.sin_family;
  myAddress.sin_addr.s_addr = address.sin_addr.s_addr;
  myAddress.sin_port = address.sin_port;

  struct timeval tv;
  tv.tv_usec = 0;  // microsecond value

  tv.tv_sec = RECEIVE_TIMEOUT_S;
  if (::setsockopt(myFDesc, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set receive timeout: " << GetErrString(errno);
    return false;
  }

  tv.tv_sec = SEND_TIMEOUT_S;
  if (::setsockopt(myFDesc, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set send timeout: " << GetErrString(errno);
    return false;
  }

  struct sigaction sa;
  sa.sa_handler = SIG_IGN;
  sa.sa_flags = 0;
  if (sigaction(SIGPIPE, &sa, 0) == -1) {
    LOG(ERROR) << "Unable to ignore SIGPIPE: " << GetErrString(errno);
    return false;
  }

  if (!SetBlocking(false)) {
    return false;
  }

  return true;
}

bool Socket::SetBlocking(bool blocking) {
  int optionVal = blocking ? OFF : ON;
  int resultCode = ioctl(myFDesc, (int)FIONBIO, (char *)&optionVal);

  if (resultCode < 0) {
    LOG(ERROR) << "Unable to set blocking: " << GetErrString(errno);
  }

  return resultCode >= 0;
}

Socket::AddressInfo Socket::GetPeerName(int fDesc) {
  socklen_t len;
  struct sockaddr_storage addr;
  char ipstr[INET6_ADDRSTRLEN];
  int port;

  AddressInfo info;

  len = sizeof addr;
  if (::getpeername(fDesc, (struct sockaddr *)&addr, &len) < 0) {
    info.valid = false;
    return info;
  }

  // deal with both IPv4 and IPv6:
  if (addr.ss_family == AF_INET) {
    struct sockaddr_in *s = (struct sockaddr_in *)&addr;
    port = ntohs(s->sin_port);
    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
  } else {  // AF_INET6
    struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
    port = ntohs(s->sin6_port);
    inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
  }

  info.address = ipstr;
  info.port = port;
  info.valid = true;

  return info;
}

bool Socket::Send(int fDesc, const std::string &message) {
  if (message.empty()) {
    return false;
  }
  uint numSent = 0;
  int status = 1;

  while ((status > 0 || errno == EAGAIN) &&
         numSent < message.length()) {
    status = ::send(fDesc, message.c_str(), message.size(), MSG_NOSIGNAL);
    numSent += status;
  }

  return status > 0;
}

bool Socket::Read(int fDesc, std::stringstream *output) {
  int status = 0;
  char buffer[READ_BUFFER_SIZE];
  bool received = false;

  do {
    // avoids buffer contamination by prev messages
    memset(&buffer, 0, sizeof(buffer));
    status = ::recv(fDesc, buffer, sizeof(buffer) - 1, MSG_WAITALL);
    if (buffer[0] != 0) {
      *output << buffer;
      received = true;
    }
  } while (status > 0);

  if (status < 0 && errno != EAGAIN) {
    LOG(ERROR) << "Receive failed: " << GetErrString(errno);
    return false;
  }

  return received;
}
