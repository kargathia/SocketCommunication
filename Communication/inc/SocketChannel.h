#pragma once

#include <atomic>
#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include "ICommunicator.h"
#include "SocketClient.h"
#include "SocketException.h"
#include "SocketServer.h"

class SocketChannel : public ICommunicator {
 public:
  explicit SocketChannel(int localPort = 0);
  virtual ~SocketChannel();

 public:  // ICommunicator functions
  void Start();
  void Send(const Message& message, const std::string& destination,
            int destinationPort);
  Message SendWReply(const Message& message, const std::string& destination,
                     int destinationPort, int timeoutS = 5);
  void AddListener(std::weak_ptr<ISimpleMessageListener> listener);

 public:
  std::string GetLocal() const;
  std::string GetAddress() const { return myAddress; }

 private:
  void RunServer();
  void RunClient();
  void NotifyListeners();
  std::string HandleMessage(int fDesc, std::string message);
  bool WaitMessage(std::string* message);

 private:
  void ClientConnect(SocketClient* client, const std::string& destination,
                     int destinationPort);
  void ClientSend(SocketClient* client, const Message& message);
  void ClientGetReply(SocketClient* client, Message* reply, int attempts);

 private:
  std::string myAddress;
  int myPort;

  std::mutex myListenerQueueMtx;
  std::vector<std::weak_ptr<ISimpleMessageListener>> myQueuedListeners;
  std::vector<std::weak_ptr<ISimpleMessageListener>> myListeners;

  std::vector<std::string> myMessages;
  std::condition_variable myMessageCV;
  std::mutex myMessageMtx;

  std::thread myServerThread;
  std::thread myHandlerThread;
  std::atomic_bool isRunning;

 private:  // disable copy constructor
  SocketChannel(SocketChannel&);
};
