// SocketException class

#pragma once

#include <string>

class SocketException {
 public:
  explicit SocketException(std::string s) : m_s(s) {}
  ~SocketException() {}

  std::string description() { return m_s; }

 private:
  std::string m_s;
};
