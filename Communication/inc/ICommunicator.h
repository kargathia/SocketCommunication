#pragma once

#include <memory>
#include <string>
#include "IMessageListener.h"
#include "Message.h"

class ICommunicator {
 public:
  virtual std::string GetLocal() const = 0;
  virtual void AddListener(std::weak_ptr<ISimpleMessageListener> listener) = 0;
  virtual void Send(const Message& message, const std::string& destination,
                    int destinationPort) = 0;
};
