#pragma once

#include <string>
#include "Message.h"

class ISimpleMessageListener {
 public:
  virtual void OnMessage(const Message& message) = 0;
};

class IReplyMessageListener {
 public:
  virtual void OnMessage(const Message& message, Message* reply);
};
