#pragma once

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string>

class Socket {
 protected:
  Socket();

 public:
  virtual ~Socket();
  bool CheckValid(int desc);

  struct AddressInfo {
    bool valid;
    std::string address;
    int port;
  };

 public:  // Getters
  bool IsValid() const { return myFDesc != -1; }
  int GetFDesc() const { return myFDesc; }
  const sockaddr_in& GetAddr() const { return myAddress; }
  AddressInfo GetPeerName(int fDesc);

 protected:
  void Disconnect();
  bool Configure();
  bool Configure(const sockaddr_in& address);
  bool Configure(const sockaddr_in& address, int fDesc);
  bool Read(int fDesc, std::stringstream* output);
  bool Send(int fDesc, const std::string& message);

  bool SetBlocking(bool blocking);
  void SetAddr(sockaddr_in address) { myAddress = address; }
  std::string GetErrString(int err) const;

 private:
  int Create();

 private:
  int myFDesc;
  sockaddr_in myAddress;
};
