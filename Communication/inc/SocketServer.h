#pragma once

#include <netinet/in.h>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include "IMessageListener.h"
#include "Socket.h"

class pollfd;

class SocketServer : public Socket {
 public:
  SocketServer();
  virtual ~SocketServer();

  bool Bind(int port);
  bool Poll(int timeoutMs);
  void SetMessageHandler(std::function<std::string(int, std::string)> handler) {
    myMessageHandler = handler;
  }

 private:
  int Accept();
  bool Insert(int newFD);
  void CompressPolledArray();
  void ClosePolledFD(pollfd* polled);

 private:
  struct pollfd polledFDs[200];
  int numPolledFDs;
  std::function<std::string(int, std::string)> myMessageHandler;
};
