#pragma once

#include <ctime>
#include <string>

class Message {
 public:
  struct MessageAddress {
    bool known = false;
    std::string address;
    int port;
  };

 public:
  std::string GetContent() const { return myContent; }
  void SetContent(const std::string& content) { myContent = content; }
  time_t GetTimestamp() { return myTimestamp; }
  const MessageAddress& GetSenderAddress() { return mySenderAddress; }

 public:
  explicit Message(std::string content)
      : myContent(content), myTimestamp(time(0)) {}
  virtual ~Message() {}

 private:
  std::string myContent;
  MessageAddress mySenderAddress;
  time_t myTimestamp;
};
